<?php

/**
 * @file
 * Hide the Revision log field.
 */

function _option_cfs_newadmin(&$form, &$form_state, $settings, $node) {
  if ($settings['cfs_newadmin'] == 0 && empty($form['cid']['value']) && user_access('administer comments')) {
    global $user;

    // Hide author info.
    $form['author']['#access'] = FALSE;

    // Wrap the fields in a fieldset.
    $form['admin'] = array(
      '#type' => 'fieldset',
      '#title' => t('Administration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => -2,
    );

    // Only allow registered users to be used.
    $form['admin']['author'] = array(
      '#type' => 'textfield',
      '#title' => t('Authored by'),
      '#size' => 30,
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => (!empty($form_state['comment']->name)) ? $form_state['comment']->name : $user->name,
      '#weight' => -1,
    );

    // Date field.
    $form['admin']['date'] = array(
      '#type' => 'textfield',
      '#parents' => array('date'),
      '#title' => t('Authored on'),
      '#size' => 20,
      '#maxlength' => 25,
      '#default_value' => (!empty($form_state['comment']->created)) ? format_date($form_state['comment']->created, 'custom', 'Y-m-d H:i O') : format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i O'),
      '#weight' => -1,
    );

    // Status field.
    $form['admin']['status'] = array(
      '#type' => 'radios',
      '#parents' => array('status'),
      '#title' => t('Status'),
      '#default_value' => (isset($form_state['comment']->status)) ? $form_state['comment']->status : 1,
      '#options' => array(1 => t('Published'), 0 => t('Not published')),
      '#weight' => -1,
    );

    // Comment presave does not carry the above form fields, so use validate here.
    $form['#validate'][] = '_option_cfs_newadmin_validate';
  }

  return $form;
}

/**
 * Function callback to handle comment form submit and save new fields data.
 *
 * @param $form Array
 *  An array of form elements
 * @param $form_state
 *  An array of form state elements
 */
function _option_cfs_newadmin_validate($form, &$form_state) {
  // Set author override.
  $author = trim($form_state['input']['author']);

  // Make sure we have an author information entered by user.
  // If author is empty, the current user is taken in case of comment add.
  // If author is empty, the current value remains in case of comment edit.
  if ($author) {
    // For anonymous user, uid is zero.
    if (strtolower($author) == 'anonymous') {
      $form_state['values']['uid'] = 0;
      $form_state['values']['name'] = $author;
    }
    else {
      // If auth user, load the user by name.
      $users = user_load_multiple(array(), array('name' => $author, 'status' => '1'));
      // Get account of this user.
      $account = reset($users);

      // If entered author is not available, then show invalid user error.
      if (!empty($account->uid)) {
        $form_state['values']['uid'] = $account->uid;
        $form_state['values']['name'] = $account->name;
      }
      else {
        form_set_error('author', t('Please enter a valid user'));
      }
    }
  }

  // If date is empty, the current time is taken in case of comment add.
  // If date is empty, the current value remains in case of comment edit.
  // Set date override.
  if (trim($form_state['input']['date'])) {
    // Get date in timestamp format.
    $date = strtotime($form_state['input']['date']);

    // Validate for a valid date.
    if ($date && $date > 0 && is_numeric($date)) {
      $form_state['values']['created'] = $date;
      $form_state['values']['changed'] = $date;
      $form_state['values']['date'] = $form_state['input']['date'];
      // Comment created date is taken from comment object and
      // not from form_state['values'], so filling the comment object here.
      $form_state['comment']->date = $form_state['input']['date'];
    }
    else {
      form_set_error('date', t('Please enter a valid date'));
    }
  }

  // Set status override always.
  $form_state['values']['status'] = $form_state['input']['status'];
}
