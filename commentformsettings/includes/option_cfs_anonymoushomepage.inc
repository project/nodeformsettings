<?php

/**
 * @file
 * Hide the Anonymous comment - homepage field.
 */

function _option_cfs_anonymoushomepage(&$form, &$form_state, $settings, $node) {
  if (isset($form['author'], $form['author']['homepage']) && $settings['cfs_anonymoushomepage'] == 1) {
    $form['author']['homepage']['#access'] = FALSE;
  }

  return $form;
}
