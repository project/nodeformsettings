<?php

/**
 * @file
 * Hide the Revision log field.
 */

function _option_cfs_comment_cancel(&$form, &$form_state, $settings, $node) {
  if ($settings['cfs_comment_cancel'] == 0 && arg(0) == "comment" && arg(1) == "reply" && is_numeric(arg(2))) {
    // User landed on comment form by clicking on a link on the node.
    $path = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);

    // If referrer has query string, use that same in the link.
    if (!empty(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY))) {
      // Get query string and parse into array.
      $query_string = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
      parse_str($query_string, $query);

      $options = array(
        'class' => 'form-button form-button-cancel',
        'rel' => 'nofollow',
      );
      $form['actions']['cancel'] = array(
        '#markup' => l(t('Cancel'), $path, array('attributes' => $options, 'query' => $query)),
        '#weight' => 51,
      );
    }
    // If user directly lands on comment page or refreshes the page, we do not
    // get referrer. In that case, use node path directly.
    else {
      $path = 'node/' . $node->nid;
      $form['actions']['cancel'] = array(
        '#markup' => l(t('Cancel'), $path),
        '#weight' => 51,
      );
    }
  }

  return $form;
}
