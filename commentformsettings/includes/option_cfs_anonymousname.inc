<?php

/**
 * @file
 * Hide the Anonymous comment - name field.
 */

function _option_cfs_anonymousname(&$form, &$form_state, $settings, $node) {
  if (isset($form['author'], $form['author']['name']) && $settings['cfs_anonymousname'] == 1) {
    $form['author']['name']['#access'] = FALSE;
  }

  return $form;
}
