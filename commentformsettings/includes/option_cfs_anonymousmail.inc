<?php

/**
 * @file
 * Hide the Anonymous comment - email field.
 */

function _option_cfs_anonymousmail(&$form, &$form_state, $settings, $node) {
  if (isset($form['author'], $form['author']['mail']) && $settings['cfs_anonymousmail'] == 1) {
    $form['author']['mail']['#access'] = FALSE;
  }

  return $form;
}
