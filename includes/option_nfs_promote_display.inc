<?php

/**
 * Hide the promote to front page checkbox if available.
 */
function _option_nfs_promote_display(&$form, &$form_state, $settings, $node) {
  if ($settings['nfs_promote_display'] == 1) {
    $form['options']['promote']['#access'] = FALSE;
  }
}
