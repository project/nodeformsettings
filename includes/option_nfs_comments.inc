<?php

/**
 * Hide comments settings vertical tab.
 */
function _option_nfs_comments(&$form, &$form_state, $settings, $node) {
  if ($settings['nfs_comments'] == 1) {
    $form['comment_settings']['#access'] = FALSE;
  }

  return $form;
}
