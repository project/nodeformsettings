<?php

/**
 * Change the page title when editing a node.
 */
function _option_nfs_title_edit($form, &$form_state, $settings, $node) {
  if (!empty($node->nid) && isset($form['#node_edit_form'])) {
    $replace_pairs = array(
      '!node_title' => $node->title,
      '!node_type' => t(node_type_get_name($node)),
    );

    $title = trim($settings['nfs_title_edit']);

    // If title is available, then change it.
    if (trim($title)) {
      $title = t(check_plain($title), $replace_pairs);
      drupal_set_title($title);
    }
  }

  return $form;
}
