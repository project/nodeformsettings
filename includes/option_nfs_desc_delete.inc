<?php

/**
 * Change the page description when deleting a node.
 */
function _option_nfs_desc_delete(&$form, &$form_state, $settings, $node) {
  if (!empty($node->nid) && !isset($form['#node_edit_form'])) {
    $replace_pairs = array('!node_title' => $node->title);

    $desc = trim($settings['nfs_desc_delete']);

    // If title is available, then change it.
    if (!empty($desc)) {
      $desc = t(check_plain($desc), $replace_pairs);
      $form['description']['#markup'] = $desc;
    }
  }
}
