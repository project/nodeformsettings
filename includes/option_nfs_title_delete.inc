<?php

/**
 * Change the page title when deleting a node.
 */
function _option_nfs_title_delete($form, &$form_state, $settings, $node) {
  if (!empty($node->nid) && !isset($form['#node_edit_form'])) {
    $replace_pairs = array(
      '!node_title' => $node->title,
      '!node_type' => t(node_type_get_name($node)),
    );

    $title = trim($settings['nfs_title_delete']);

    // If title is available, then change it.
    if (!empty($title)) {
      $title = t(check_plain($title), $replace_pairs);
      drupal_set_title($title);
    }
  }

  return $form;
}
