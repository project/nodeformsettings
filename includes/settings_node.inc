<?php

/**
 * Define all settings for nodes.
 */
function _nodeformsettings_settings_form(&$form, $settings = NULL) {
  // The module is going to work with or without ctools.
  if (module_exists('ctools')) {
    ctools_include('dependent');
  }
  $defaults = nodeformsettings_elements_default();

  $options_en_ds = array(0 => t('Enabled'), 1 => t('Disabled'));

  $options_yes_no = array(0 => t('Yes'), 1 => t('No'));

  $form['nodeformsettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node form settings'),
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );

  // Hide the Input Form Fieldset.
  $form['nodeformsettings']['nfs_inputformat'] = array(
    '#title' => t('Body field input text format'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_inputformat']) ? $settings['nfs_inputformat'] : $defaults['nfs_inputformat'],
  );

  // Get form elements for cancel link.
  _nodeformsettings_settings_cancel_link_form($form, $settings, $options_yes_no);

  // Change the submit button text value.
  $form['nodeformsettings']['nfs_submit'] = array(
    '#title' => t('Submit button value'),
    '#type' => 'textfield',
    '#description' => t('Leave blank to restore default'),
    '#default_value' => isset($settings['nfs_submit']) ? $settings['nfs_submit'] : $defaults['nfs_submit'],
  );

  // Get form elements for vertical tab settings.
  _nodeformsettings_settings_vertical_tabs_form($form, $settings, $options_en_ds);

  // Get form elements for titles and description settings.
  _nodeformsettings_settings_titles_desc_form($form, $settings, $options_yes_no);

  // Get form elements for publishing options settings.
  _nodeformsettings_settings_publishing_options_form($form, $settings, $options_en_ds);

  $form['nodeformsettings']['set_default_to_all'] = array(
    '#type' => 'submit',
    '#value' => t('Set these node form settings as default'),
    '#submit' => array('nodeformsettings_set_default_to_all'),
    '#suffix' => '<div>' . t('Set these settings as default for all the content types') . '</div>',
  );

  $form['nodeformsettings']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset node form settings to default'),
    '#submit' => array('nodeformsettings_reset_default'),
    '#suffix' => '<div>' . t('Reset to default node form settings will clear custom changes to these settings.') . '</div>',
  );
  return $form;
}

function _nodeformsettings_settings_cancel_link_form(&$form, $settings, $options_yes_no) {
  // Show a Cancel button.
  $form['nodeformsettings']['nfs_cancel'] = array(
    '#tree' => TRUE,
  );

  $form['nodeformsettings']['nfs_cancel']['nfs_cancel_status'] = array(
    '#title' => t('Add Cancel link'),
    '#type' => 'radios',
    '#options' => $options_yes_no,
    '#description' => t("Display Cancel link in node add/edit form next to buttons at the bottom of the page"),
    '#default_value' => isset($settings['nfs_cancel'], $settings['nfs_cancel']['nfs_cancel_status']) ? $settings['nfs_cancel']['nfs_cancel_status'] : $defaults['nfs_cancel']['nfs_cancel_status'],
  );

  $form['nodeformsettings']['nfs_cancel']['nfs_cancel_behaviour'] = array(
    '#title' => t('Cancel link behaviour'),
    '#type' => 'select',
    '#options' => array(
      0 => t('Back one page using JavaScript'),
      1 => t('Using previous destination'),
    ),
    '#default_value' => isset($settings['nfs_cancel'], $settings['nfs_cancel']['nfs_cancel_behaviour']) ? $settings['nfs_cancel']['nfs_cancel_behaviour'] : $defaults['nfs_cancel']['nfs_cancel_behaviour'],
  );

  if (module_exists('ctools')) {
    $form['nodeformsettings']['nfs_cancel']['nfs_cancel_behaviour']['#dependency'] = array('radio:nfs_cancel[nfs_cancel_status]' => array(0));
    $form['nodeformsettings']['nfs_cancel']['nfs_cancel_behaviour']['#process'] = array('ctools_dependent_process');
  }
}

function _nodeformsettings_settings_vertical_tabs_form(&$form, $settings, $options_en_ds) {
  // Default vertical tab.
  $vertical_tabs = array(
    '-none-' => t('- Leave default -'),
    'author' => t('Author'),
    'comment_settings' => t('Comment settings'),
    'menu' => t('Menu'),
    'options' => t('Publishing options'),
    'path' => t('Path'),
    'revision_information' => t('Revision information'),
    'xmlsitemap' => t('Xmlsitemap'),
  );
  // Vertical tabs fieldset settings.
  $form['nodeformsettings']['vertical_tabs'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Vertical tabs settings'),
  );
  // Vertical tab default view.
  $form['nodeformsettings']['vertical_tabs']['nfs_default_vertical_tab'] = array(
    '#title' => t('Default enabled vertical tab'),
    '#type' => 'select',
    '#options' => $vertical_tabs,
    '#default_value' => isset($settings['nfs_default_vertical_tab']) ? $settings['nfs_default_vertical_tab'] : $defaults['nfs_default_vertical_tab'],
  );
  // Hide the Revision log message.
  $form['nodeformsettings']['vertical_tabs']['nfs_menu_settings'] = array(
    '#title' => t('Menu settings fieldset'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_menu_settings']) ? $settings['nfs_menu_settings'] : $defaults['nfs_menu_settings'],
  );
  // Hide the Revision log message.
  $form['nodeformsettings']['vertical_tabs']['nfs_revisionlog'] = array(
    '#title' => t('Revision log message fieldset'),
    '#type' => 'radios',
    '#options' => array(
      '0' => t('Enabled (if revisions are enabled)'),
      '1' => t('Only show when user has <a href="!url">View Revisions permission</a>', array('!url' => url('admin/user/permissions', array('fragment' => 'edit-1-view-revisions-wrapper')))),
      '2' => t('Disabled (for all users)'),
    ),
    '#default_value' => isset($settings['nfs_revisionlog']) ? $settings['nfs_revisionlog'] : $defaults['nfs_revisionlog'],
  );

  // Hide the Author Information Fieldset.
  $form['nodeformsettings']['vertical_tabs']['nfs_author_information'] = array(
    '#title' => t('Author information fieldset'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_author_information']) ? $settings['nfs_author_information'] : $defaults['nfs_author_information'],
  );

  // Hide the Path Fieldset.
  $form['nodeformsettings']['vertical_tabs']['nfs_path'] = array(
    '#title' => t('Path fieldset'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_path']) ? $settings['nfs_path'] : $defaults['nfs_path'],
  );
  // Hide the Comments options Fieldset.
  $form['nodeformsettings']['vertical_tabs']['nfs_comments'] = array(
    '#title' => t('Comments options fieldset'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_comments']) ? $settings['nfs_comments'] : $defaults['nfs_comments'],
  );
  // Option to hide the xmlsitemap inclusion settings.
  if (module_exists('xmlsitemap_node')) {
    $form['nodeformsettings']['vertical_tabs']['nfs_sitemap_display'] = array(
      '#type' => 'radios',
      '#title' => t('XML Sitemap fieldset'),
      '#options' => $options_en_ds,
      '#default_value' => isset($settings['nfs_sitemap_display']) ? $settings['nfs_sitemap_display'] : $defaults['nfs_sitemap_display'],
    );
  }
}

function _nodeformsettings_settings_titles_desc_form(&$form, $settings, $options_yes_no) {
  // Titles and text fieldset settings.
  $form['nodeformsettings']['texts'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Titles & descriptions settings'),
  );

  // Option to hide the node title.
  $form['nodeformsettings']['texts']['nfs_hide_node_title'] = array(
    '#type' => 'radios',
    '#title' => t('Show node title'),
    '#options' => $options_yes_no,
    '#description' => t("This setting controls whether or not the node title is displayed when viewing the node. The title field is going to appear on the 'create node' form regardless of these settings. To avoid its display and automatically create nodetitles you can use the module !url", array('!url' => l(t('Automatic Nodetitles'), 'http://drupal.org/project/auto_nodetitle', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => isset($settings['nfs_hide_node_title']) ? $settings['nfs_hide_node_title'] : $defaults['nfs_hide_node_title'],
  );

  // Change the title of the form when creating a node (node/add).
  $form['nodeformsettings']['texts']['nfs_title_create'] = array(
    '#title' => t('Page title when creating a node'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['nfs_title_create']) ? $settings['nfs_title_create'] : $defaults['nfs_title_create'],
    '#description' => t('Available variable: !node_type. Leave blank to restore default'),
  );

  // Change the title of the form when editing a node (node/edit).
  $form['nodeformsettings']['texts']['titles']['nfs_title_edit'] = array(
    '#title' => t('Page title when editing a node'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['nfs_title_edit']) ? $settings['nfs_title_edit'] : $defaults['nfs_title_edit'],
    '#description' => t('Available variables: !node_title, !node_type. Leave blank to restore default'),
  );

  // Change the title of the form when deleting a node (node/nid/delete).
  $form['nodeformsettings']['texts']['nfs_title_delete'] = array(
    '#title' => t('Page title when deleting a node'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['nfs_title_delete']) ? $settings['nfs_title_delete'] : $defaults['nfs_title_delete'],
    '#description' => t('Available variables: !node_title, !node_type. Leave blank to restore default'),
  );

  // Change the description of the form when deleting a node (node/nid/delete).
  $form['nodeformsettings']['texts']['nfs_desc_delete'] = array(
    '#title' => t('Page description when deleting a node'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['nfs_desc_delete']) ? $settings['nfs_desc_delete'] : $defaults['nfs_desc_delete'],
    '#description' => t('Available variables: !node_title. Leave blank to restore default'),
  );
  $form['nodeformsettings']['texts']['nfs_title_description'] = array(
    '#title' => t('Help text for title field'),
    '#type' => 'textarea',
    '#description' => t('Leave blank to restore default'),
    '#default_value' => isset($settings['nfs_title_description']) ? $settings['nfs_title_description'] : $defaults['nfs_title_description'],
  );

  // Set the Help text for body field.
  $form['nodeformsettings']['texts']['nfs_body_description'] = array(
    '#title' => t('Help text for body field'),
    '#type' => 'textarea',
    '#description' => t('Leave blank to restore default'),
    '#default_value' => isset($settings['nfs_body_description']) ? $settings['nfs_body_description'] : $defaults['nfs_body_description'],
  );
}

function _nodeformsettings_settings_publishing_options_form(&$form, $settings, $options_en_ds) {
  // Publising options fieldset settings.
  $form['nodeformsettings']['publishing'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Publishing options settings'),
  );
  // Hide the Publishing options Fieldset.
  $form['nodeformsettings']['publishing']['nfs_publishingoptions'] = array(
    '#title' => t('Publishing options fieldset'),
    '#type' => 'radios',
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_publishingoptions']) ? $settings['nfs_publishingoptions'] : $defaults['nfs_publishingoptions'],
  );
  // Option to hide the promote to front page.
  $form['nodeformsettings']['publishing']['nfs_promote_display'] = array(
    '#type' => 'radios',
    '#title' => t('Promote to front page checkbox'),
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_promote_display']) ? $settings['nfs_promote_display'] : $defaults['nfs_promote_display'],
  );

  // Option to hide the sticky at the top of the list.
  $form['nodeformsettings']['publishing']['nfs_sticky_display'] = array(
    '#type' => 'radios',
    '#title' => t('Sticky at top of lists checkbox'),
    '#options' => $options_en_ds,
    '#default_value' => isset($settings['nfs_sticky_display']) ? $settings['nfs_sticky_display'] : $defaults['nfs_sticky_display'],
  );
}
