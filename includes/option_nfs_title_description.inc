<?php

/**
 * Set the node title description (help text).
 */
function _option_nfs_title_description(&$form, &$form_state, $settings, $node) {
  // Only proceed if the title element exists.
  if (!isset($form['title'])) {
    return $form;
  }

  $desc = t(check_plain(trim($settings['nfs_title_description'])));

  // Set help text for title field.
  if ($desc) {
    $form['title']['#description'] = $desc;
  }

  return $form;
}
