<?php

/**
 * Hide the sticky at top of list checkbox if available.
 */
function _option_nfs_sticky_display(&$form, &$form_state, $settings, $node) {
  if ($settings['nfs_sticky_display'] == 1) {
    $form['options']['sticky']['#access'] = FALSE;
  }
}
