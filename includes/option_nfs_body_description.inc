<?php

/**
 * Set the node body description (help text).
 */
function _option_nfs_body_description(&$form, &$form_state, $settings, $node) {
  // Only proceed if the body_field element exists.
  if (!isset($form['body'])) {
    return $form;
  }

  $desc = t(check_plain(trim($settings['nfs_body_description'])));

  // Set help text for body field.
  if ($desc) {
    $lang = 'und';
    if (isset($form['body']['#language'])) {
      $lang = $form['body']['#language'];
    }

    $form['body'][$lang][0]['#description'] = $desc;
  }

  return $form;
}
