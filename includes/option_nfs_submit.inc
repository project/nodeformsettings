<?php

/**
 * Change the value for the submit button.
 */
function _option_nfs_submit(&$form, &$form_state, $settings, $node) {
  if (!empty($settings['nfs_submit'])) {
    $title = trim($settings['nfs_submit']);
    $form['actions']['submit']['#value'] = t(check_plain($title));
  }

  return $form;
}
