<?php

/**
 * Change the page title when creating a node.
 */
function _option_nfs_title_create($form, &$form_state, $settings, $node) {
  if (empty($node->nid)) {
    $replace_pairs = array('!node_type' => t(node_type_get_name($node)));

    $title = trim($settings['nfs_title_create']);

    // If title is available, then change it.
    if (trim($title)) {
      $title = t(check_plain($title), $replace_pairs);
      drupal_set_title($title);
    }
  }

  return $form;
}
