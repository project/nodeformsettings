<?php

/**
 * Hide xml sitemap settings vertical tab.
 */
function _option_nfs_sitemap_display(&$form, &$form_state, $settings, $node) {
  if ($settings['nfs_sitemap_display'] == 1) {
    $form['xmlsitemap']['#access'] = FALSE;
  }
}
