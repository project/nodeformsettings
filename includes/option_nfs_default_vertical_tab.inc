<?php

/**
 * Set the default vertical tab in node add/edit page.
 */
function _option_nfs_default_vertical_tab(&$form, &$form_state, $settings, $node) {
  if (isset($form[$settings['nfs_default_vertical_tab']])) {
    $form[$settings['nfs_default_vertical_tab']]['#weight'] = -50;
  }
}
