<?php

/**
 * Hide menu settings vertical tab.
 */
function _option_nfs_menu_settings(&$form, &$form_state, $settings, $node) {
  if ($settings['nfs_menu_settings'] == 1) {
    $form['menu']['#access'] = FALSE;
  }

  return $form;
}
